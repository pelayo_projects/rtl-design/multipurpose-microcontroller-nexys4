
library ieee;
use ieee.std_logic_1164.all;

entity edgeDetector is
  port (
    rst_n : in  std_logic;   
    clk   : in  std_logic;   
    x_n   : in  std_logic;  
    xFall : out std_logic;   
    xRise : out std_logic    
  );
end edgeDetector;



architecture Behavioral of edgeDetector is 
begin

  process (rst_n, clk)
    variable aux1, aux2: std_logic;
  begin
    xFall <= (not aux1) and aux2;
    xRise <= aux1 and (not aux2);
    if rst_n='0' then
      aux1 := '1';
      aux2 := '1';
    elsif rising_edge(clk) then
      aux2 := aux1;
      aux1 := x_n;           
    end if;
  end process;

end Behavioral;
