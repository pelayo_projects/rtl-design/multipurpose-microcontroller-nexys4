library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.STD_LOGIC_UNSIGNED.ALL;

entity adder is
port(    op1 : in  std_logic_vector(7 downto 0);
         op2: in  std_logic_vector(7 downto 0);
         resul : out std_logic_vector(7 downto 0)
);
end adder;

architecture rtl of adder is

begin

  resul <= op1 + op2;

end rtl;