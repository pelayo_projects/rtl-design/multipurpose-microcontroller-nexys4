library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

use work.common.all;

entity rs232Receiver is
    generic (
        FREQ     : in natural := 50_000_000;  
        BAUDRATE : in natural := 115200
    );
    port (
        clk     : in  std_logic;  
        rst_n   : in  std_logic;  
        rx      : in  std_logic ;  
        dataRdy : out std_logic;   
        data    : out std_logic_vector (7 downto 0)  
    );
end rs232Receiver;

architecture Behavioral of rs232Receiver is 
    
    constant MAXVALUE  : natural := ((FREQ/BAUDRATE) - 1);
    
    signal bitPos     : natural range 0 to 10;  
    signal rxRegShift : std_logic_vector (7 downto 0);
    signal rxRead, baudCntCE: std_logic;
    
    signal count : std_logic_vector(log2(MAXVALUE) - 1 downto 0);
    signal tc, clear : std_logic;
    

begin
        
    baudCnt: modCounter
        generic map( MAXVALUE => MAXVALUE )
        port map( clk => clk, rst_n => rst_n, clear => clear, ce => baudCntCE, tc => tc, count => count);

    clear <= '1' when baudCntCE = '0' else '0';
    rxRead <= '1' when to_integer(unsigned(count)) = MAXVALUE/2 else '0';

    p_fsm:
    process(rst_n, clk, rxRegShift, bitPos, rxRead)
    begin
    data      <= rxRegShift;
    baudCntCE <= '1';
    dataRdy   <= '0';
    bitPos <= bitPos;
    if bitPos = 0 then
        baudCntCE <= '0';
    end if;
    if bitPos = 10  and  rxRead = '1' then
        dataRdy <= '1';
    end if;
     
    if rst_n = '0' then
        bitPos <= 0;
        rxRegShift <= (others => '1');
    elsif rising_edge(clk) then
        case bitPos is
        when 0 => 
            if rx = '0' then
                bitPos <= 1;
            end if;   
        when 1 => 
            if rxRead = '1' then
                bitPos <= 2;
            end if;
        when 10 => 
            if rxRead = '1' then
				bitPos <= 0;
			end if;
        when others => 
            if rxRead = '1' then
                bitPos <= bitPos + 1;
                rxRegShift <= rx & rxRegShift(7 downto 1);	
            end if;
        end case;
    end if;
    end process;

end Behavioral;