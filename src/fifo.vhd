library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

use work.common.all;

entity fifo is
    generic(
         WIDTH : in natural := 8;
         DEPTH : in natural := 5
    );
    Port ( 
        rst_n   : in std_logic;
        clk     : in std_logic;
        wr      : in std_logic;
        rd      : in std_logic;
        din     : in std_logic_vector(WIDTH - 1 downto 0);  
        full    : out std_logic;
        empty   : out std_logic;
        dout    : out std_logic_vector(WIDTH - 1 downto 0)
    );
end fifo;

architecture Behavioral of fifo is

    constant max : natural := DEPTH-1;
    type regFifoType is array (0 to max) of std_logic_vector(WIDTH-1 downto 0);
    
    signal regFifo : regFifoType;   
    signal wrPointer, rdPointer : natural range 0 to max;
    signal isFull, isEmpty: std_logic := '0';
    
    signal nextWrPointer, nextRdPointer : natural range 0 to max;
    signal rdFifo, wrFifo : std_logic;

begin

  wrFifo <= '1' when wr= '1' and isFull = '0' else '0';
  rdFifo <= '1' when rd= '1' and isEmpty = '0' else '0';
  
  nextWrPointer <= wrPointer + 1 when wrPointer < max else 0;
  nextRdPointer <= rdPointer + 1 when rdPointer < max else 0;
    
  p_regFifo:
  process (clk, rst_n, rdPointer, regFifo)
  begin
    dout <= regFifo(rdPointer);
    if rst_n='0' then
      regFifo <= (others => (others => '0'));
    elsif rising_edge(clk) then
        if wrFifo = '1' then
	       regFifo(wrPointer) <= din;
	    end if;
    end if;
  end process;
    
  p_fsm:
  process (rst_n, clk) 
  begin     
    if rst_n = '0' then
      wrPointer <= 0;
      rdPointer <= 0;
      isFull    <= '0';
      isEmpty   <= '1';
    elsif rising_edge(clk) then
      if wrFifo = '1' then
        wrPointer <= nextWrPointer;
	      if rdFifo='0' and nextWrPointer = rdPointer then
		      isFull <= '1';
		  end if;
		  isEmpty <= '0';
      end if;
      if rdFifo = '1' then
        rdPointer <= nextRdPointer;
		  if wrFifo = '0' and nextRdPointer = wrPointer then
		      isEmpty <= '1';
		  end if;
		  isFull <= '0';
      end if;
    end if;
  end process;
   
  full <= isFull;
  empty <= isEmpty;
    

end Behavioral;