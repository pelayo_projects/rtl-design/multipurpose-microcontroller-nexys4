
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

use work.common.all;

entity bin2segs is
    generic(
        FREQ : in natural := 50_000_000
    );
    port(
        clk   : in  std_logic;
        rst_n : in  std_logic;
        load  : in  std_logic;
        dp    : in  std_logic;
        bin   : in  std_logic_vector(31 downto 0);
        an    : out std_logic_vector(7 downto 0);
        segs  : out std_logic_vector(7 downto 0)
    );
end bin2segs;

architecture Behavioral of bin2segs is 

    type regFileType is array (0 to 7) of std_logic_vector(3 downto 0); -- 8 registros de 4 bits cada uno(8 displays);
    
    constant MAXVALUE : natural := ((FREQ/1000)) - 1;
    signal memRegs : regFileType;
    signal pointerReg : natural range 0 to 7;

    signal tc : std_logic;
    signal AN_reg : std_logic_vector(7 downto 0);

begin

    AN <= AN_reg;

    counter : modCounter
    generic map (MAXVALUE => MAXVALUE)
    port map( clk => clk, rst_n => rst_n, clear => '0', ce => '1', tc => tc, count => open );


    p_regValues:
    process(clk,rst_n)
    begin
    if rst_n = '0' then
            memRegs <= (others => (others => '0'));
        elsif rising_edge(clk) then
            if load = '1' then
                for i in 0 to 7 loop
                   memRegs(i) <= bin(((4 * (i+1)) - 1) downto (4*i));
                end loop;
            end if;
        end if;
    end process;


    p_regPinter:
    process(clk,rst_n)
    begin
        if rst_n = '0' then
            pointerReg <= 0;
        elsif rising_edge(clk)then
            if tc = '1' then
                if load = '0'  then
                    if pointerReg = 7 then 
                        pointerReg <= 0;
                    else 
                        pointerReg <= pointerReg + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;   

    p_regAN:
    process(clk,rst_n)
    begin
        if rst_n = '0' then
            AN_reg <= (others => '0');
        elsif rising_edge(clk) then
            if tc = '1' then
                if pointerReg = 7 then
                    AN_reg <= "11111110";
                else 
                    AN_reg <= AN_reg(6 downto 0) & '1';
                end if;
            end if;
        end if;
    end process;       


    segs(7) <= tc; 
    with  memRegs(pointerReg) select
    segs(6 downto 0) <= 
    "1000000" when X"0",
    "1111001" when X"1",
    "0100100" when X"2",
    "0110000" when X"3",
    "0011001" when X"4",
    "0010010" when X"5",
    "0000010" when X"6",
    "1111000" when X"7",
    "0000000" when X"8",
    "0011000" when X"9",
    "0001000" when X"A",
    "0000011" when X"B",
    "0100111" when X"C",
    "0100001" when X"D",
    "0000110" when X"E",
    "0001110" when X"F",
    "0000000" when others;

end Behavioral;