
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

use work.common.all;
use work.pic.all;

entity lab4 is
    port ( 
        osc       : in std_logic;
        rstPb_n   : in std_logic;
        rx        : in  std_logic;        
        tx        : out std_logic;       
        switches  : out std_logic_vector(7 downto 0); 
        segs      : out std_logic_vector(7 downto 0); 
        hSync     : out std_logic;
        vSync     : out std_logic;
        rgb       : out std_logic_vector(11 downto 0);  
        an        : out std_logic_vector(7 downto 0)
    );   
end lab4;

architecture Behavioral of lab4 is

    signal clk, rst_n : std_logic;
    signal readRx, validTx, txRdy, full, empty, write_en, oe, ack, send, ackDma, ready, dmaRq, FlagZ, FlagC, FlagN, FlagE: std_logic;
    signal dataRx, dataTx, addr, temp, databus, indexReg: std_logic_vector(7 downto 0);

    signal aluOp : ALU_op;
    signal romData, romAddr:std_logic_vector(11 downto 0);
    
    signal oeDma, write_enDma, oeCpu,  write_enCpu, cntl: std_logic;
    signal addrCpu, addrDma: std_logic_vector(7 downto 0);
    
    constant FREQ :  natural := 50_000_000;
    
     component clk_gen is
        port(
            clk_in1  : in std_logic;
            resetn   : in std_logic;
            clk_out1 : out std_logic
        );
    end component;
    

begin    
    
    oe <= oeDma when (cntl = '1') else oeCpu;
    write_en <= write_enDma when (cntl = '1' ) else write_enCpu;
    addr <=  addrDma when (cntl = '1') else addrCpu;
    
   
    rst_n <= not rstPb_n;
    --rst_n <= rstPb_n;
    --clk <=  osc;
      
     clk_gen50Mhz : clk_gen 
     port map(
        clk_in1   => osc,
        resetn    => rst_n,
        clk_out1  => clk
     );

     rsDriver :entity work.rs232top
        generic map(FREQ => FREQ)
        port map (
            clk     => clk, 
            rst_n   => rst_n, 
            rx      => rx, 
            readRx  => readRx, 
            validTx => validTx, 
            dataTx  => dataTx,
            txRdy   => txRdy,
            ack     => ack,
            tx      => tx,
            full    => full,
            empty   => empty,
            dataRx  => dataRx
        );
     memory : ram
        generic map( FREQ => FREQ )
        port map (
            clk      => clk,
            rst_n    => rst_n,
            write_en => write_en,
            oe       => oe,
            addr     => addr,
            temp     => temp,
            switches => switches,
            segs     => segs, 
            an       => an,
            databus  => databus
         );
     dmaDriver : dma
        port map(
            clk      => clk,
            rst_n    => rst_n,
            rxFull   => full,
            rxEmpty  => empty,
            ackTx    => ack,
            txRdy    => txRdy,
            send     => send,
            ackDMA   => ackDma,
            dataRx   => dataRx,
            cntl     => cntl,
            readRx   => readRx,
            validTx  => validTx,
            dataTx   => dataTx,
            ready    => ready,
            dmaRq    => dmaRq,
            cs       => open,
            oe       => oeDma,
            write_en => write_enDma,
            addr     => addrDma,
            databus  => databus
        );
        
    cpuControl : cpu
        port map(
           Reset     => rst_n,
           Clk       => clk,
           ROM_Data  => romData,
           ROM_Addr  => romAddr,
           RAM_Addr  => addrCpu,
           RAM_Write => write_enCpu,
           RAM_OE    => oeCpu,
           Databus   => databus,
           DMA_RQ    => dmaRq,
           DMA_ACK   => ackDma,
           SEND_comm => send,
           DMA_READY => ready,
           Alu_op    => aluOp,
           Index_Reg => indexReg,
           FlagZ     => FlagZ,
           FlagC     => FlagC,
           FlagN     => FlagN,
           FlagE     => FlagE
        );
    
    aluRISC : alu
        port map(
            clk      => clk,
            rst_n    => rst_n,
            aluOp    => aluOp,
            indexReg => indexReg,
            flagZ    => flagZ,
            flagC    => flagC, 
            flagN    => flagN,
            flagE    => flagE,
            databus  => databus
        );
   romMemory: entity work.ROM 
       port map (
          Instruction => romData,
          Program_counter => romAddr          
        );
   
   vgaI : entity work.vgaInterface
     port map(
        clk => clk,
        rst_n  => rst_n,
        temp_H  => temp(7 downto 4),
        temp_L  => temp(3 downto 0),
        hSync   => hSync,
        vSync   => vSync,
        rgb     => rgb
     );


end Behavioral;