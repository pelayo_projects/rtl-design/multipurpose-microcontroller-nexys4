library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

use work.pic.all;
use work.common.all;

entity ram is
    generic (
        FREQ : in natural := 50_000_000
    );
    port (
        clk      : in  std_logic;
        rst_n    : in  std_logic;
        write_en : in  std_logic;
        oe       : in  std_logic;
        addr     : in  std_logic_vector(7 downto 0);
        temp     : out std_logic_vector(7 downto 0);
        switches : out std_logic_vector(7 downto 0);
        segs     : out std_logic_vector(7 downto 0);   
        an       : out std_logic_vector(7 downto 0);
        databus  : inout std_logic_vector(7 downto 0)
    );
end entity;

architecture Behavioral of ram is
    
    type array_ram is array (integer range <>) of std_logic_vector (7 downto 0);
    
    signal ram_ES : array_ram(63 downto 0);
    signal ram_PG : array_ram(191 downto 0);
   
    type states is (diff,refresh);
    signal currentState, nextState: states;
  
  
   signal load : std_logic;
   signal bin, regbin  : std_logic_vector(31 downto 0);
   
   signal dmaRxBufferMsb, dmaRxBufferMid, dmaRxBufferLsb, dmaRxBufferInst: std_logic_vector(7 downto 0);
   

begin
    
    
    p_switches:
            for i in 0 to 7 generate
                switches(i) <= ram_ES(to_integer(unsigned(unsigned(SWITCH_BASE))) + i)(0);
            end generate;
            
    p_map_ramES:
    process(clk, rst_n)
    begin
        if rst_n = '0' then
            ram_ES <= (others => (others => '0'));
            ram_ES(to_integer(unsigned(T_STAT))) <= X"20";
        elsif rising_edge(clk) then
            if addr < GP_RAM_BASE and write_en = '1' then
                ram_ES(to_integer(unsigned(addr))) <= databus;
            end if;
        end if;
    end process;
    
    p_map_ramPG:
    process(clk)
    begin
        if rising_edge(clk) then
            if addr >= GP_RAM_BASE and write_en = '1' then
                ram_PG(to_integer(unsigned(addr) - unsigned(((GP_RAM_BASE))))) <= databus;
            end if;
        end if;
    end process; 
 
 
databus <= ram_ES(to_integer(unsigned(addr))) when oe = '0' and addr < GP_RAM_BASE else 
           ram_PG(to_integer(unsigned(addr) - unsigned(((GP_RAM_BASE))))) when oe = '0' and addr >= GP_RAM_BASE else
           (others => 'Z');
    
    dmaRxBufferMsb  <= ram_ES(to_integer(unsigned(((DMA_RX_BUFFER_MSB)))));
    dmaRxBufferMid  <= ram_ES(to_integer(unsigned(((DMA_RX_BUFFER_MID)))));
    dmaRxBufferLsb  <= ram_ES(to_integer(unsigned(((DMA_RX_BUFFER_LSB)))));
    dmaRxBufferInst <= ram_ES(to_integer(unsigned(((NEW_INST)))));
    temp <= ram_ES(to_integer(unsigned(((T_STAT)))));
    
    p_fsm_stateGen:
    process (regbin, bin, currentState, dmaRxBufferMsb, dmaRxBufferMid, dmaRxBufferLsb, dmaRxBufferInst)
    begin
        regbin <=  dmaRxBufferMsb & dmaRxBufferMid & dmaRxBufferLsb & dmaRxBufferInst;
        nextState <= currentState;
        load <= '0';
        case currentState is
            when diff    => 
                if regbin /= bin then
                   nextState <= refresh;
                end if;
            when refresh =>      
                    load <= '1';
                    nextState <= diff;
            end case;
    end process;

    p_fsm_state:
    process(rst_n, clk)
    begin
        if rst_n = '0' then
            currentState <= diff;
        elsif rising_edge(clk) then
            currentState <= nextState;
        end if;
    end process;

    converter : bin2segs 
        generic map(FREQ => FREQ)
        port map( clk  => clk, rst_n => rst_n, load => load, dp => '0', bin => bin , an => an, segs => segs ); 

    process(clk, rst_n)
    begin
        if rst_n = '0' then
            bin <= (others => '0');
        elsif rising_edge(clk) then
            if regbin /= bin then
                bin  <= regbin;
            end if;
        end if;
    end process;

end Behavioral;




