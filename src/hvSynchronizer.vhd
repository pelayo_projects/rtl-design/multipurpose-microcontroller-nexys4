
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

use work.common.all;

entity hvSynchronizer is
    generic(
        FREQ      : in natural := 50_000_000;  
        SYNCDELAY : in natural := 0
        );
    port(
        clk      : in std_logic;
        rst_n    : in std_logic;
        hSync    : out std_logic;
        vSync    : out std_logic;
        blanking : out std_logic;
        pixel    : out std_logic_vector(9 downto 0);
        line     : out std_logic_vector(9 downto 0)
    );
end hvSynchronizer;

architecture Behavioral of hvSynchronizer is
    
    constant CYCLESxPIXEL : natural := (FREQ/25_000_000) - 1;
    constant PIXELSxLINE  : natural  := 800 - 1; 
    constant LINESxFRAME  : natural  := 525 - 1;
    
    signal tc_CxP, tc_PxL, tc_LxF: std_logic;
    
    signal pixelCnt : std_logic_vector(9 downto 0);
    signal lineCnt  : std_logic_vector(9 downto 0); 
    
    signal count : natural range 0 to CYCLESxPIXEL;
begin

      divisor:
      process (rst_n, clk)
      begin
      if rst_n='0' then
         count <= 0;
      elsif rising_edge(clk) then
        if count = CYCLESxPIXEL then
			count <= 0;
        else
            count <= count + 1;
        end if;
      end if;
      end process;
     
    tc_CxP <= '1' when  count = CYCLESxPIXEL else '0';

    counter_PxL : modCounter
    generic map (MAXVALUE => PIXELSxLINE)
    port map( clk => clk, rst_n => rst_n, clear => '0', ce => tc_CxP, tc => tc_PxL, count => pixelCnt);

    counter_LxF : modCounter
    generic map (MAXVALUE => LINESxFRAME)
    port map( clk => clk, rst_n => rst_n, clear => '0', ce => tc_PxL, tc => tc_LxF, count => lineCnt);   
  
    hSync <= '0' when (unsigned(pixelCnt) >= 656 + SYNCDELAY and unsigned(pixelCnt) < 752 + SYNCDELAY) else '1';
    vSync <= '0' when (unsigned(lineCnt) >= 494 and unsigned(lineCnt) < 496) else '1';  
    
    blanking <= '0' when (unsigned(pixelCnt) < SYNCDELAY) or (unsigned(lineCnt) >= 480) or (unsigned(pixelCnt) >= 640 + SYNCDELAY) else '1';
    
    pixel <= pixelCnt;
    line <= lineCnt;

end Behavioral;
