library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity mult8b is
    port(
        clk: in std_logic;
        rst: in std_logic;
         X : in  std_logic_vector(3 downto 0);
         Y : in  std_logic_vector(3 downto 0);
         S : out std_logic_vector(7 downto 0)
    );
end mult8b;
architecture rtl of mult8b is

signal sum1, sum1reg, sum2, sum2Reg, op1, op2, op3, op3Reg, op4, op4Reg, op4RegReg: std_logic_vector(7 downto 0);


  component adder
    port(op1 : in  std_logic_vector(7 downto 0);
         op2 : in  std_logic_vector(7 downto 0);
         resul : out std_logic_vector(7 downto 0));
  end component;
  
  
  component registerV is
    port(
    clk: in std_logic;
    rst: in std_logic;
    din:    in std_logic_vector(7 downto 0);
    dout:   out std_logic_vector(7 downto 0)
    );
    end component;

begin
    
    op1 <= "0000" & (X and (Y(0) & Y(0) & Y(0) & Y(0)));
    op2 <= "000" & (X and (Y(1) & Y(1) & Y(1) & Y(1))) & '0';
    op3 <= "00" & (X and (Y(2) & Y(2) & Y(2) & Y(2))) & "00";
    op4 <= '0' & (X and (Y(3) & Y(3) & Y(3) & Y(3))) & "000";
    
    adder1 : adder
        port map ( 
            op1 => op1,
            op2 => op2,
            resul => sum1
        );
        
    registerV1 : registerV
        port map ( 
            clk => clk,
            rst => rst,
            din => sum1,
            dout => sum1reg
     );
     
     registerV2 : registerV
        port map ( 
            clk => clk,
            rst => rst,
            din => op3,
            dout => op3Reg
     );
     
    adder2 : adder
        port map ( 
            op1 => sum1reg,
            op2 => op3Reg,
            resul => sum2
       );
       
   registerV3 : registerV
        port map ( 
            clk => clk,
            rst => rst,
            din => sum2,
            dout => sum2Reg
     );
     
   registerV4 : registerV
        port map ( 
            clk => clk,
            rst => rst,
            din => op4,
            dout => op4Reg
     );
     
   registerV5 : registerV
        port map ( 
            clk => clk,
            rst => rst,
            din => op4Reg,
            dout => op4RegReg
     );
     
     
    adder3 : adder
        port map ( 
            op1 => sum2Reg,
            op2 => op4RegReg,
            resul => S
       );
  
end rtl;