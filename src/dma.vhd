
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

use work.pic.all;
  

entity dma is
    port(
        clk     : in std_logic;
        rst_n   : in std_logic;
        rxFull  : in std_logic;
        rxEmpty : in std_logic;
        ackTx   : in std_logic;
        txRdy   : in std_logic;
        send    : in std_logic;
        ackDMA  : in std_logic;
        dataRx  : in std_logic_vector(7 downto 0);
        cntl    : out std_logic;
        readRx  : out std_logic;
        validTx : out std_logic;
        dataTx  : out std_logic_vector(7 downto 0);
        ready   : out std_logic;
        dmaRq   : out std_logic;
        cs      : out std_logic;
        oe      : out std_logic;
        write_en: out std_logic;
        addr    : out std_logic_vector(7 downto 0);
        databus : inout std_logic_vector(7 downto 0)
    );
end dma;

architecture Behavioral of dma is
    
    type regDmaRxBuffer is array (0 to 3) of std_logic_vector(7 downto 0);
    type regDmaTxBuffer is array (0 to 1) of std_logic_vector(7 downto 0);
    
    constant dmaRxBuffer : regDmaRxBuffer := (0 => DMA_RX_BUFFER_MSB, 1 => DMA_RX_BUFFER_MID, 2 => DMA_RX_BUFFER_LSB, 3 => NEW_INST);
    constant dmaTxBuffer : regDmaTxBuffer := (0 => DMA_TX_BUFFER_MSB, 1 => DMA_TX_BUFFER_LSB);
    type states is (init, rxControl, txControl);
    signal currentState, nextState: states;
    
    signal dmaRxBuffPointer : natural range 0 to 3;
    signal dmaTxBuffPointer : natural range 0 to 1;
    
    signal wAddrRx : std_logic;
    
begin

    cntl <= '1' when (currentState /= init) else '0';    
    p_receiver_fsmd:
    process(currentState, rxEmpty, ackDMA, dataRx, send, txRdy, databus, dmaRxBuffPointer, dmaTxBuffPointer, ackTx)
    begin
        nextState <= currentState;
        databus  <= (others => 'Z');
        addr     <= (others => '0');
        dataTx   <= (others => '0');
        dmaRq    <= '0';
        readRx   <= '0';
        write_en <= '0';
        wAddrRx  <= '0';
        oe       <= '1';
        ready    <= '1';
        validTx  <= '0';
        case currentState is
        when init => 
            if rxEmpty = '0' and ackDMA = '1' then
                nextState <= rxControl;
                dmaRq <= '1';
            elsif send = '1' then 
                ready <= '0';
                nextState <= txControl;
            elsif rxEmpty = '0' then
                dmaRq <= '1';
                nextState <= init;
            end if;
        when rxControl => 
            dmaRq    <= '1';
            readRx   <= '1';
            wAddrRx  <= '1';   
            oe       <= '1';
            write_en <= '1';
            addr     <=  dmaRxBuffer(dmaRxBuffPointer);
            if dmaRxBuffer(dmaRxBuffPointer) = DMA_RX_BUFFER_LSB then
                nextState <= rxControl;
                dataBus  <= dataRx;
            elsif dmaRxBuffer(dmaRxBuffPointer) = NEW_INST then
                dataBus  <= X"FF";
                nextState <= init;
            else 
                dataBus  <= dataRx;
                nextState <= init;
            end if;
        when txControl =>
            oe    <= '0';
            ready <= '0';
            if txRdy = '1' then 
                addr <= dmaTxBuffer(dmaTxBuffPointer);
                dataTx <= dataBus;
                validTx <= '1';
            end if;
            if dmaTxBuffer(dmaTxBuffPointer) = DMA_TX_BUFFER_LSB and ackTx = '1' then
                nextState <= init;
                ready <= '1';
            else
                 nextState <= txControl;
            end if;
        end case; 
    end process;
    
    
    p_addrTx:
    process(rst_n, clk)
    begin
        if rst_n = '0' then
            dmaTxBuffPointer <= 0;
        elsif rising_edge(clk) then
            if ackTx = '1' and currentState = txControl then 
                if dmaTxBuffPointer = 1 then 
                    dmaTxBuffPointer <= 0;
                else
                    dmaTxBuffPointer <= dmaTxBuffPointer + 1;
                end if;
            end if;
        end if;
    end process;
    
    p_addrRx:
    process(rst_n, clk)
    begin
        if rst_n = '0' then
            dmaRxBuffPointer <= 0;
        elsif rising_edge(clk) then
            if wAddrRx = '1' and currentState = rxControl then 
                if dmaRxBuffPointer = 3 then
                    dmaRxBuffPointer <= 0;
                else
                    dmaRxBuffPointer <= dmaRxBuffPointer + 1;
                end if;
            end if;
        end if;
    end process;

    
    p_fsm_receiverState:
    process(rst_n, clk)
    begin
        if rst_n = '0' then
            currentState <= init;
        elsif rising_edge(clk) then
            currentState <= nextState;
        end if;
    end process;


end Behavioral;
