
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

use work.pic.all;

entity alu is
    port (   
        clk      : in     std_logic;
        rst_n    : in     std_logic;
        aluOp    : in     alu_op;
        indexReg : out    std_logic_vector (7 downto 0);
        flagZ    : out    std_logic;
        flagC    : out    std_logic;
        flagN    : out    std_logic;
        flagE    : out    std_logic;
        databus  : inout  std_logic_vector (7 downto 0)
      );
end alu;

architecture Behavioral of alu is

    signal regA, regB, regACC, regINDEX : std_logic_vector(7 downto 0);
    signal regFLAGS : std_logic_vector(3 downto 0);
    signal opOut, opComplexOut : unsigned(7 downto 0);
    signal flagZaux, flagCaux, flagNaux, flagEaux : std_logic;
    

begin

    opOut <=  (unsigned(regA) + unsigned(regB))   when (aluOp = op_add)    else
			  (unsigned(regA) - unsigned(regB))   when (aluOp = op_sub)    else
			  (unsigned(unsigned(regACC) sll 1))  when (aluOp = op_shiftl) else
              (unsigned(unsigned(regACC) srl 1)) when (aluOp = op_shiftr) else
		      (unsigned(regA) and unsigned(regB)) when (aluOp = op_and)    else
			  (unsigned(regA) or  unsigned(regB)) when (aluOp = op_or)     else
		      (unsigned(regA) xor unsigned(regB)) when (aluOp = op_xor)    else
			  opComplexOut;
    
    p_opComplexOut:
    process(aluOp, regA, regACC)
    begin
        case aluOp is 
            when op_ascii2bin => if unsigned(regA) >= 48 and unsigned(regA) <= 57 then opComplexOut <= (unsigned(regA) - 48); else opComplexOut <= X"FF"; end if;
            when op_bin2ascii => if unsigned(regA) >= 0 and unsigned(regA)  <= 9  then opComplexOut <= (unsigned(regA) + 48); else opComplexOut <= X"FF"; end if;
            --when nop => opComplexOut <= (others => '0');
            when others =>	opComplexOut <= unsigned(regACC);
	    end case;
    end process;  
    
    p_opFLAGZ:
    process(aluOp, opOut, regA, regB, regINDEX)
    begin
        flagZaux <=  '0';
        case aluOp is
            when op_cmpe => if unsigned(regA) = unsigned(regB) then flagZaux <= '1'; else flagZaux <=  '0'; end if;
            when op_cmpl => if unsigned(regA) < unsigned(regB) then flagZaux <= '1'; else flagZaux <=  '0'; end if;
            when op_cmpg => if unsigned(regA) > unsigned(regB) then flagZaux <= '1'; else flagZaux <=  '0'; end if;
            --when nop => flagZaux <= flagZaux;
            when others  => if (opOut = X"00")  then flagZaux <= '1'; else flagZaux <= '0'; end if; 
        end case;
    end process;
    
    p_opFLAGC_N:
    process(aluOp, opOut, regA, regB)
    begin
        case aluOp is
            when op_add => if (opOut < unsigned(regA)) or (opOut < unsigned(regB)) then flagCaux <= '1'; else flagCaux <=  '0'; end if;
                           if (opOut(3 downto 0) < unsigned(regA(3 downto 0))) or (opOut(3 downto 0) < unsigned(regB(3 downto 0))) then flagNaux <= '1'; else flagNaux <=  '0'; end if;    
            when op_sub => if unsigned(regA) < unsigned(regB) then flagCaux <= '1'; else flagCaux <=  '0'; end if;
                           if unsigned(regA(3 downto 0)) < unsigned(regB(3 downto 0)) then flagNaux <= '1'; else flagNaux <=  '0'; end if;
            when others => flagNaux <= '0';  flagCaux <=  '0';
        end case;
    end process;
    
    p_opFLAGE:
    flagEaux <=  '1'  when ((aluOp = op_ascii2bin or aluOp = op_bin2ascii) and opOut = X"FF")  else '0';
    
    
    
        
    databus <= regACC when (aluOp = op_oeacc) else (others =>'Z');
    indexReg <= regINDEX;
    flagZ   <= regFLAGS(3);
    flagC   <= regFLAGS(2);
    flagN   <= regFLAGS(1);
    flagE   <= regFLAGS(0);
     
     
    --regFLAGS <= flagZaux & flagCaux & flagNaux & flagEaux;
    p_regs:
    process(clk, rst_n)
    begin
        if rst_n = '0' then
            regA     <= (others => '0');
            regB     <= (others => '0');
            regACC   <= (others => '0');
            regINDEX <= (others => '0');
           regFLAGS  <= (others => '0');
        elsif rising_edge(clk) then
            
            case aluOp is
                when op_lda       => regA     <= databus;
                when op_ldb       => regB     <= databus;
                when op_ldacc     => regACC   <= databus;
                when op_ldid      => regINDEX <= databus;
                when op_mvacc2id  => regINDEX <= regACC;
                when op_mvacc2a   => regA     <= regACC;
                when op_mvacc2b   => regB     <= regACC;
                when nop => 			
                when others => regACC <= std_logic_vector(opOut); regFLAGS <= flagZaux & flagCaux & flagNaux & flagEaux;
		    end case;
		end if;
    end process;
    
end Behavioral;
