
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity synchronizer is
 generic (
    STAGES  : in natural;      
    INIT    : in std_logic     
  );
  port (
    rst_n : in  std_logic;  
    clk   : in  std_logic;   
    x     : in  std_logic;   
    xSync : out std_logic    
  );
end synchronizer;

architecture Behavioral of synchronizer is

begin
    
  process (rst_n, clk)
    variable aux : std_logic_vector(STAGES-1 downto 0); 
  begin
    xSync <= aux(STAGES-1);		
    if rst_n='0' then
      aux := (others => INIT);
    elsif rising_edge(clk) then
      for i in STAGES-1 downto 1 loop
        aux(i) := aux(i-1);
      end loop;
      aux(0) := x;
    end if;
  end process;


end Behavioral;
