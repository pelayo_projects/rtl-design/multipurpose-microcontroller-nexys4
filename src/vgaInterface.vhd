
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

use work.common.all;

entity vgaInterface is
     port (
        clk     : in std_logic;
        rst_n   : in std_logic;
        temp_H  : in std_logic_vector;
        temp_L  : in std_logic_vector;
        hSync   : out std_logic;
        vSync   : out std_logic;
        rgb     : out std_logic_vector(11 downto 0)
     );
end vgaInterface;

architecture Behavioral of vgaInterface is

    signal line, pixel : std_logic_vector(9 downto 0);
    signal blanking : std_logic;
    
    signal addr : std_logic_vector(11 downto 0);
    signal data : std_logic_vector(7 downto 0);
    
    constant trevol : std_logic_vector(11 downto 0) := X"060";
    
    type regFontsAddrL is array (0 to 19) of std_logic_vector(11 downto 0);
    type regFontsAddrN is array (0 to 9) of std_logic_vector(11 downto 0);
    
    constant badrVga : regFontsAddrL := (0 => X"420", 1 => X"410", 2 => X"440", 3 => X"450", 4 => X"520", 5 => X"000",
        6 => X"470", 7 => X"550", 8 => X"410", 9 => X"490", 10 => X"540", 11 => X"4F0", 12 => X"550", 13 => X"4E0", 14 => X"450", 15 => X"000",
        16 => X"410", 17 => X"4B0", 18 => X"440", 19 => X"490");
        
    constant pelayoVga : regFontsAddrL := (0 => X"500", 1 => X"450", 2 => X"4C0", 3 => X"410", 4 => X"590", 5 => X"4F0",6 => X"000", 
        7 => X"4C0", 8 => X"450", 9 => X"470", 10 => X"550", 11 => X"490", 12 => X"4E0", 13 => X"410", 14 => X"000", 
        15 => X"4C0", 16 => X"4F0", 17 => X"500", 18 => X"450", 19 => X"5A0");
        
    signal numberVga : regFontsAddrL;
      
    constant tmpVga : regFontsAddrN := (0 => X"300", 1 => X"310", 2 => X"320", 3 => X"330", 4 => X"340", 5 => X"350", 6 => X"360", 7 => X"370", 8 => X"380", 9 => X"390");

begin
    
       
    process(rst_n,clk)
    begin
        if rst_n = '0' then
            numberVga <= (others => (others => '0'));
            numberVga(9) <= tmpVga(to_integer(unsigned(temp_H)));
            numberVga(10) <= tmpVga(to_integer(unsigned(temp_L)));
       elsif rising_edge(clk) then
            numberVga(9) <= tmpVga(to_integer(unsigned(temp_H)));
            numberVga(10) <= tmpVga(to_integer(unsigned(temp_L)));
       end if;
   end process;
    
    screenInteface: hvSynchronizer
        generic map ( FREQ => 50_000_000, SYNCDELAY => 0 )
        port map ( clk => clk, rst_n => rst_n, blanking => blanking, pixel => pixel, line => line, hSync => hSync, vSync => vSync);

    
    fonts : fontsRom 
        port map(addr => addr, data => data);
        
        
    addr <= std_logic_vector(unsigned(pelayoVga(to_integer((unsigned(pixel) / 8) - 31))) + (unsigned(line) - 15))  when (unsigned(unsigned(line) / 15) = 1) else
            std_logic_vector(unsigned(badrVga(to_integer((unsigned(pixel) / 8) - 31))) + (unsigned(line) - (15*2))) when (unsigned(unsigned(line) / 15) = 2) else
            std_logic_vector(unsigned(numberVga(to_integer((unsigned(pixel) / 8) - 31))) + (unsigned(line) - (15*16))) when (unsigned(unsigned(line) / 15) = 16) else
            "000000000000";
    
    process(line,pixel,blanking, data)
    begin
        rgb <= "000000000000";
        if (unsigned(unsigned(pixel) / 8) >= 31 and unsigned(unsigned(pixel) / 8) <= 50) and blanking = '1' then
             if data(to_integer(unsigned(pixel(2 downto 0)))) = '1' then
                rgb <= "111111111111";
             else 
                rgb <= "000000000000";
             end if;
        elsif  blanking = '0'  then
            rgb <= "000000000000";
        else 
            rgb <= "000000000000";
        end if;    
    end process;



end Behavioral;
