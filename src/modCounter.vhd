
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

use work.common.all;

entity modCounter is
    generic (
        MAXVALUE : in natural  
    );
    port (  
        clk   : in  std_logic; 
        rst_n : in  std_logic;   
        clear : in  std_logic;   
        ce    : in  std_logic;   
        tc    : out std_logic;   
        count : out std_logic_vector(log2(MAXVALUE)-1 downto 0) 
    );
end modCounter;

architecture Behavioral of modCounter is

  signal cs : unsigned(count'range);

begin

    p_counter:
    process (rst_n, clk)
    begin
    if rst_n = '0' then
        cs <= (others => '0');
    elsif rising_edge(clk) then
        if clear = '1' then cs <= (others =>'0');
        elsif ce = '1' then
            if cs = MAXVALUE then
                cs <= (others =>'0');
            else 
                cs <= cs + 1;
            end if;
        end if;
    end if;
    end process;

    count <= std_logic_vector(cs);


    tc <=  '1' when cs = MAXVALUE and ce = '1' else  '0'; 


end Behavioral;