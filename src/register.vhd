

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity registerV is
 port(
    clk: in std_logic;
    rst: in std_logic;
    din:    in std_logic_vector(7 downto 0);
    dout:   out std_logic_vector(7 downto 0)
    );
    
end registerV;

architecture Behavioral of registerV is


begin

    process(clk, rst)
    begin
        if rst = '0' then
            dout <= "00000000";
       elsif rising_edge(clk) then
            dout <= din;
       end if;
    end process;

end Behavioral;
