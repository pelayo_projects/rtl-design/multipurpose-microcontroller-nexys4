library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

use work.common.all;

entity rs232Transmitter is
    generic (
        FREQ     : in natural := 50_000_000;  
        BAUDRATE : in natural := 115200
    );
    port (
        clk     : in  std_logic;   
        rst_n   : in  std_logic;  
        dataRdy : in  std_logic;   
        data    : in  std_logic_vector (7 downto 0);  
        ack     : out std_logic;  
        busy    : out std_logic;  
        tx      : out std_logic   
    );
end rs232Transmitter;


architecture Behavioral of rs232Transmitter is

    constant MAXVALUE  : natural := ((FREQ/BAUDRATE) - 1);

    signal bitPos : natural range 0 to 10;   
    signal rxRegShift : std_logic_vector(9 downto 0);
  
    signal baudCntCE, txWrite : std_logic;
    signal tc, clear : std_logic;

begin

    baudCnt: modCounter
        generic map( MAXVALUE => MAXVALUE )
        port map( clk => clk, rst_n => rst_n, clear => clear, ce => baudCntCE, tc => tc, count => open);
       
    clear <= '1' when baudCntCE = '0' else '0'; 
    txWrite <= '1' when tc = '1' else '0';
    
    fsmd :
    process (rst_n, clk, bitPos, rxRegShift, dataRdy)
    begin
    tx      <= rxRegShift(0);
    baudCntCE <= '1';
    busy      <= '1';
    ack       <= '0';
    if bitPos = 0 then
       baudCntCE <= '0';
       busy      <= '0';
       if dataRdy = '1' then
          ack        <= '1';
       end if;
    end if;
    if rst_n = '0' then
        rxRegShift <= (others =>'1'); 
        bitPos <= 0;
    elsif rising_edge(clk) then
        case bitPos is
        when 0 =>                  
            if dataRdy = '1' then
		      rxRegShift <= '1' & data & '0';
			  bitPos <= 1;
			end if;
        when 10 =>                                                       
            if txWrite = '1' then
              bitPos <= 0;
			  rxRegShift <= '1' & rxRegShift( 9 downto 1);
			end if;
        when others =>                      
            if txWrite = '1' then
              bitPos <= bitPos + 1;
		      rxRegShift <= '1' & rxRegShift( 9 downto 1);
			end if;
    end case;
    end if;
  end process;
    
end Behavioral;