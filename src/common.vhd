
library IEEE;
  use IEEE.STD_LOGIC_1164.ALL;
  use IEEE.NUMERIC_STD.ALL;
  
use work.pic.all;

package common is

constant YES  : std_logic := '1';
constant NO   : std_logic := '0';
constant HI   : std_logic := '1';
constant LO   : std_logic := '0';
constant ONE  : std_logic := '1';
constant ZERO : std_logic := '0';

-- Calcula el logaritmo en base-2 de un numero.
function log2(v : in natural) return natural;
-- Selecciona un entero entre dos.
function int_select(s : in boolean; a : in integer; b : in integer) return integer;
-- Convierte un real en un signed en punto fijo con qn bits enteros y qm bits decimales. 
function toFix( d: real; qn : natural; qm : natural ) return signed; 
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component bin2segs is
    generic(
      FREQ : in natural := 50_000_000
    );
    port(
      clk   : in  std_logic;
      rst_n : in  std_logic;
      load  : in  std_logic;
      dp    : in  std_logic;
      bin   : in  std_logic_vector(31 downto 0);
      an    : out std_logic_vector(7 downto 0);
      segs  : out std_logic_vector(7 downto 0)
    );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component modCounter is
    generic (
      MAXVALUE : in natural  
    );
    port (  
      clk   : in  std_logic; 
      rst_n : in  std_logic;   
      clear : in  std_logic;   
      ce    : in  std_logic;   
      tc    : out std_logic;   
      count : out std_logic_vector(log2(MAXVALUE)-1 downto 0) 
    );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component rs232Receiver is
  generic (
      FREQ     : in natural := 50_000_000;  
      BAUDRATE : in natural := 115200
  );
  port (
      clk     : in  std_logic;  
      rst_n   : in  std_logic;  
      rx      : in  std_logic ;  
      dataRdy : out std_logic;   
      data    : out std_logic_vector (7 downto 0)  
  );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component rs232Transmitter is
  generic (
      FREQ     : in natural := 50_000_000;  
      BAUDRATE : in natural := 115200
  );
  port (
      clk     : in  std_logic;
      rst_n   : in  std_logic;     
      dataRdy : in  std_logic;   
      data    : in  std_logic_vector (7 downto 0);
      ack     : out std_logic;     
      busy    : out std_logic;  
      tx      : out std_logic   
  );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component fifo is
    generic(
      WIDTH : in natural := 8;
      DEPTH : in natural := 5
    );
    port ( 
      clk     : in std_logic;
      rst_n   : in std_logic;
      wr      : in std_logic;
      rd      : in std_logic;
      din     : in std_logic_vector(WIDTH - 1 downto 0);  
      full    : out std_logic;
      empty   : out std_logic;
      dout    : out std_logic_vector(WIDTH - 1 downto 0)
    );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component synchronizer is
 generic (
    STAGES  : in natural;      
    INIT    : in std_logic     
  );
  port (
    rst_n : in  std_logic;  
    clk   : in  std_logic;   
    x     : in  std_logic;   
    xSync : out std_logic    
  );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component ram is
    generic (
      FREQ : in natural := 50_000_000
    );
    port (
        clk      : in  std_logic;
        rst_n    : in  std_logic;
        write_en : in  std_logic;
        oe       : in  std_logic;
        addr     : in  std_logic_vector(7 downto 0);
        temp     : out std_logic_vector(7 downto 0);
        switches : out std_logic_vector(7 downto 0);
        segs      : out std_logic_vector(7 downto 0);   
        an        : out std_logic_vector(7 downto 0);
        databus  : inout std_logic_vector(7 downto 0)
    );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component dma is
    port(
        clk     : in std_logic;
        rst_n   : in std_logic;
        rxFull  : in std_logic;
        rxEmpty : in std_logic;
        ackTx   : in std_logic;
        txRdy   : in std_logic;
        send    : in std_logic;
        ackDMA  : in std_logic;
        dataRx  : in std_logic_vector(7 downto 0);
        cntl    : out std_logic;
        readRx  : out std_logic;
        validTx : out std_logic;
        dataTx  : out std_logic_vector(7 downto 0);
        ready   : out std_logic;
        dmaRq   : out std_logic;
        cs      : out std_logic;
        oe      : out std_logic;
        write_en: out std_logic;
        addr    : out std_logic_vector(7 downto 0);
        databus : inout std_logic_vector(7 downto 0)
    );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component edgeDetector is
  port (
    rst_n : in  std_logic;   
    clk   : in  std_logic;  
    x_n   : in  std_logic;   
    xFall : out std_logic;  
    xRise : out std_logic   
  );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component debouncer is
  generic(
    FREQ   : natural;  
    BOUNCE : natural   
  );
  port (
    rst_n  : in  std_logic;   
    clk    : in  std_logic;   
    x_n    : in  std_logic;   
    xdeb_n : out std_logic   
  );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component alu is
  port (   
      clk      : in  std_logic;
      rst_n    : in  std_logic;
      aluOp    : in alu_op;
      indexReg : out  std_logic_vector (7 downto 0);
      flagZ    : out  std_logic;
      flagC    : out  std_logic;
      flagN    : out  std_logic;
      flagE    : out  std_logic;
      databus  : inout  std_logic_vector (7 downto 0)
    );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component cpu is
  port ( Reset : in  STD_LOGIC;
         Clk : in  STD_LOGIC;
         ROM_Data : in  STD_LOGIC_VECTOR (11 downto 0);
         ROM_Addr : out  STD_LOGIC_VECTOR (11 downto 0);
         RAM_Addr : out  STD_LOGIC_VECTOR (7 downto 0);
         RAM_Write : out  STD_LOGIC;
         RAM_OE : out  STD_LOGIC;
         Databus : inout  STD_LOGIC_VECTOR (7 downto 0);
         DMA_RQ : in  STD_LOGIC;
         DMA_ACK : out  STD_LOGIC;
         SEND_comm : out  STD_LOGIC;
         DMA_READY : in  STD_LOGIC;
         Alu_op : out  alu_op;
         Index_Reg : in  STD_LOGIC_VECTOR (7 downto 0);
         FlagZ : in  STD_LOGIC;
         FlagC : in  STD_LOGIC;
         FlagN : in  STD_LOGIC;
         FlagE : in  STD_LOGIC);
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component hvSynchronizer is
    generic(
        FREQ      : in natural := 50_000_000;  
        SYNCDELAY : in natural := 0
        );
    port(
        clk      : in std_logic;
        rst_n    : in std_logic;
        hSync    : out std_logic;
        vSync    : out std_logic;
        blanking : out std_logic;
        pixel    : out std_logic_vector(9 downto 0);
        line     : out std_logic_vector(9 downto 0)
    );
end component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
component fontsRom is
	port(
		addr  : in std_logic_vector(11 downto 0);
		data  : out std_logic_vector(7 downto 0)
	);
end  component;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
end package common;

package body common is

  function log2(v : in natural) return natural is
    variable n    : natural;
    variable logn : natural;
  begin
    n := 1;
    for i in 0 to 128 loop
      logn := i;
      exit when (n >= v);
      n := n * 2;
    end loop;
    return logn;
  end function log2;

  function int_select(s : in boolean; a : in integer; b : in integer) return integer is
  begin
    if s then
      return a;
    else
      return b;
    end if;
    return a;
  end function int_select;

  function toFix( d: real; qn : natural; qm : natural ) return signed is 
  begin 
    return to_signed( integer(d*(2.0**qm)), qn+qm );
  end function; 

end package body common;
