
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;


entity tb_rs232Receiver is
--  Port ( );
end tb_rs232Receiver;

architecture Behavioral of tb_rs232Receiver is

    constant FREQUENCY : natural := 100_000_000;   
    constant PERIOD    : time := 1 sec/FREQUENCY;
    constant BAUDRATE  : natural := 115200;

    component rs232Receiver is
    generic (
        FREQ     : in natural := 100_000_000;  
        BAUDRATE : in natural := 115200
    );
    port (
        clk     : in  std_logic;  
        rst_n   : in  std_logic;  
        rx      : in  std_logic ;  
        dataRdy : out std_logic;   
        data    : out std_logic_vector (7 downto 0)  
        
        );
    end component;
    
    signal rst_n, clk, rx, dataRdy: std_logic := '0'; 
    signal data :  std_logic_vector (7 downto 0) := (others => '0');

begin
    
    -- Connect UUT
    UUT: rs232Receiver
    generic map (FREQ => FREQUENCY, BAUDRATE => BAUDRATE)
    port map(clk => clk, rst_n => rst_n, rx => rx, dataRdy => dataRdy, data => data);

    gen_clock:
    process
    begin
        clk <= '1', '0' after PERIOD/2;
        wait for PERIOD;
    end process gen_clock;
   
    gen_rst: 
    rst_n <= '1' after 5 ns;


end Behavioral;
