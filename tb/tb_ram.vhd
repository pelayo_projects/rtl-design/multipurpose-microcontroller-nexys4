
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;


entity tb_ram is
end tb_ram;

architecture Behavioral of tb_ram is
    
   
    constant FREQ : natural := 100_000_000;  
    constant PERIOD    : time := 1 sec/FREQ; 
    
    
    component ram is
        port (
            clk      : in    std_logic;
            rst_n    : in    std_logic;
            write_en : in    std_logic;
            oe       : in    std_logic;
            addr     : in    std_logic_vector(7 downto 0);
            databus  : inout std_logic_vector(7 downto 0)
        );
    end component;
    
    signal write_en, oe, rst_n, clk : std_logic := '0'; 
    signal addr, databus: std_logic_vector(7 downto 0) := (others => '0');

begin
    
    gen_clock:
    process
    begin
        clk <= '1', '0' after PERIOD/2;
        wait for PERIOD;
    end process gen_clock;
   
    gen_rst: 
    rst_n <= '1' after 5 ns;
    
    
    gen_addr:
    process
    begin
        addr <= X"31";
       -- write_en <= '1' after 20 ns, '0' after 40 ns;
        --oe <= '1' after 20 ns, '0' after 40 ns;
        --databus <=  X"FF" after 20 ns;
        wait;
    end process gen_addr;

end Behavioral;
