
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity tb_dma is
end tb_dma;

architecture Behavioral of tb_dma is
    
    
    constant FREQ : natural := 100_000_000;  
    constant PERIOD    : time := 1 sec/FREQ; 
    
    
    component dma is
        port(
            clk     : in std_logic;
            rst_n   : in std_logic;
            rxFull  : in std_logic;
            rxEmpty : in std_logic;
            ackTx   : in std_logic;
            txRdy   : in std_logic;
            send    : in std_logic;
            ackDMA  : in std_logic;
            dataRx  : in std_logic_vector(7 downto 0);
            
            readRx  : out std_logic;
            validTx : out std_logic;
            dataTx  : out std_logic_vector(7 downto 0);
            ready   : out std_logic;
            dmaRq   : out std_logic;
            cs      : out std_logic;
            oe      : out std_logic;
            write_en: out std_logic;
            addr    : out std_logic_vector(7 downto 0);
            databus : inout std_logic_vector(7 downto 0)
        );
    end component;

    
    signal clk, rst_n, rxFull, rxEmpty, ackTx, txRdy, send, ackDMA: std_logic := '0'; 
    signal readRx, validTx, ready, dmaRq, cs, oe, write_en: std_logic := '0'; 
    signal dataRx, dataTx, addr, databus: std_logic_vector(7 downto 0) := (others => '0');

begin

    dmaController : dma 
        port map(
        clk      => clk, 
        rst_n    => rst_n, 
        rxFull   => rxFull, 
        rxEmpty  => rxEmpty, 
        ackTx    => ackTx, 
        txRdy    => txRdy, 
        send     => send,
        ackDMA   => ackDMA,
        dataRx   => dataRx,
        readRx   => readRx,
        validTx  => validTx,
        dataTx   => dataTx,
        ready    => ready,
        dmaRq    => dmaRq,
        cs       => cs,
        oe       => oe,
        write_en => write_en,
        addr     => addr,
        databus  => databus 
     ); 
    
    gen_clock:
    process
    begin
        clk <= '1', '0' after PERIOD/2;
        wait for PERIOD;
    end process gen_clock;
   
    gen_rst: 
    rst_n <= '1' after 5 ns;
    
    
    gen_addr:
    process
    begin
         --rxEmpty <= '0' after 20 ns;
         --ackDMA <= '1' after 20 ns, '0' after 200 ns;
         --dataRx <= X"0F";
         
         send  <= '1' after 20 ns;
         txRdy <= '1' after 20 ns;
         ackTx <= '1' after 20 ns;
         --oe <= '1' after 20 ns, '0' after 40 ns;
         --databus <=  X"FF" after 20 ns;
        wait;
    end process gen_addr;

end Behavioral;


    