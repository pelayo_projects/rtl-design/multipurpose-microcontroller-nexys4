
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    
    
use work.pic.all;


entity tb_alu is
end tb_alu;

architecture Behavioral of tb_alu is
    
    constant FREQ : natural := 50_000_000;  
    constant PERIOD    : time := 1 sec/FREQ; 
    
    component alu is
        port (   
            clk      : in  std_logic;
            rst_n    : in  std_logic;
            aluOp    : in alu_op;
            indexReg : out  std_logic_vector (7 downto 0);
            flagZ    : out  std_logic;
            flagC    : out  std_logic;
            flagN    : out  std_logic;
            flagE    : out  std_logic;
            databus  : inout  std_logic_vector (7 downto 0)
          );
    end component;
     
    signal aluOp : alu_op;   
    signal flagZ, flagC, flagN, flagE, rst_n, clk : std_logic := '0'; 
    signal databus, indexReg: std_logic_vector(7 downto 0) := (others => '0');
    
begin
    
    UUT : alu
        port map(
            clk      => clk,
            rst_n    => rst_n,
            aluOp    => aluOp,
            indexReg => indexReg,
            flagZ    => flagZ,
            flagC    => flagC,
            flagN    => flagN,
            flagE    => flagE,
            databus  => databus
        );
        
    gen_clock:
    process
    begin
        clk <= '1', '0' after PERIOD/2;
        wait for PERIOD;
    end process gen_clock;
   
    gen_rst: 
    rst_n <= '1' after 5 ns;
    
    stim_proc: process
   begin			
	   aluOp <= nop;
       wait for 100 ns;	
		
		-- Load A
		databus<="00000001";
		wait for 10 ns;
  		aluOp <= op_lda;
		wait for 100 ns;
  		aluOp <= nop;
		wait for 10 ns;
		databus <= (others => 'Z');
        wait for 50 ns;
		
		-- Load B
		databus<="00000001";
		wait for 10 ns;
  		aluOp <= op_ldb;
		wait for 100 ns;
  		aluOp <= nop;
		wait for 10 ns;
		databus <= (others => 'Z');  		
        wait for 50 ns;
		-- A+B
  		aluOp <= op_add;
		wait for 100 ns;
		-- A-B
		aluOp <= op_sub;
		wait for 100 ns;
		-- ShiftL
		aluOp <= op_shiftl;
		wait for 100 ns;
		-- ShiftR
		aluOp <= op_shiftr;
		wait for 100 ns;
		-- AND
		aluOp <= op_and;
		wait for 100 ns;
		-- OR
		aluOp <= op_or;
		wait for 100 ns;
		-- XOR
		aluOp <= op_xor;
		wait for 100 ns;
		-- CMPE
		aluOp <= op_cmpe;
		wait for 100 ns;
		-- CMPG
		aluOp <= op_cmpg;
		wait for 100 ns;	
		-- CMPL
		aluOp <= op_cmpl;
		wait for 100 ns;
		-- ASCII2BIN (error)
		aluOp <= op_ascii2bin;
		wait for 100 ns;
		-- Load A
		databus<="00110111";
		wait for 10 ns;
  		aluOp <= op_lda;
		wait for 100 ns;
  		aluOp <= nop;
		wait for 10 ns;
		databus<=(others => 'Z');
      wait for 50 ns;
		-- ASCII2BIN (7)
		aluOp <= op_ascii2bin;
		wait for 100 ns;
		-- Load A
		databus<="00001001";
		wait for 10 ns;
  		aluOp <= op_lda;
		wait for 100 ns;
  		aluOp <= nop;
		wait for 10 ns;
		Databus<=(others => 'Z');
      wait for 50 ns;
		-- ASCII2BIN (9)
		aluOp <= op_bin2ascii;
		wait for 100 ns;
		-- op_mvacc2id (9)
		aluOp <= op_mvacc2id;
		wait for 100 ns;
      -- insert stimulus here 
      wait;
   end process;
    

end Behavioral;
