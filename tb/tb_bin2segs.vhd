
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity tb_bin2segs is
end tb_bin2segs;

architecture Behavioral of tb_bin2segs is
    
    constant FREQUENCY : natural := 100_000_000;  
    constant FREQ: real := 100.0e6;
    constant PERIOD    : time := 1 sec/FREQ; 
    constant BAUDRATE  : natural := 1200;
    
    
    component bin2segs is
        generic(
          FREQ : in natural := 100_000_000
        );
        port(
          clk   : in  std_logic;
          rst_n : in  std_logic;
          load  : in  std_logic;
          dp    : in  std_logic;
          bin   : in  std_logic_vector(31 downto 0);
          an    : out std_logic_vector(7 downto 0);
          segs  : out std_logic_vector(7 downto 0)
        );
    end component;
    
    signal load, dp, rst_n, clk : std_logic := '0'; 
    signal bin   : std_logic_vector(31 downto 0) := (others => '0');
    signal an, segs, reg : std_logic_vector(7 downto 0) := (others => '0');


begin
    
    converter : bin2segs 
        generic map(FREQ => 100_000)
        port map( clk  => clk, rst_n => rst_n, load => load, dp => '0', bin => bin , an => an,segs => segs ); 
    
    gen_clock:
    process
    begin
        clk <= '1', '0' after PERIOD/2;
        wait for PERIOD;
    end process gen_clock;
   
    gen_rst: 
    rst_n <= '1' after 5 ns;
    
    gen_switches:
    process
    begin
     load <= '1' after 20 ns, '0' after 40 ns;
      bin  <= X"0000FFFF" after 20 ns;
    end process gen_switches;


end Behavioral;
