
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity tb_rs232Transmitter is
end tb_rs232Transmitter;

architecture Behavioral of tb_rs232Transmitter is

    constant FREQUENCY : natural := 50_000_000;  
    constant FREQ: real := 50.0e6;
    constant PERIOD    : time := 1 sec/FREQ; 
    constant BAUDRATE  : natural := 115200;


    component rs232Transmitter is
        generic (
            FREQ     : in natural := 50_000_000;  
            BAUDRATE : in natural := 115200
        );
        port (
            clk     : in  std_logic;  
            rst_n   : in  std_logic;   
            dataRdy : in  std_logic;   
            data    : in  std_logic_vector (7 downto 0);   
            busy    : out std_logic;  
            tx     : out std_logic   
        );
    end component;
    
    signal rst_n, clk, dataRdy, busy, tx: std_logic := '0'; 
    signal data :  std_logic_vector (7 downto 0) := (others => '0');

begin

    -- Connect UUT
    UUT: rs232Transmitter
    generic map (FREQ => FREQUENCY, BAUDRATE => BAUDRATE)
    port map( clk => clk, rst_n => rst_n, dataRdy => dataRdy, data => data, busy => busy, tx => tx);

    gen_clock:
    process
    begin
        clk <= '1', '0' after PERIOD/2;
        wait for PERIOD;
    end process gen_clock;
   
    gen_rst: 
    rst_n <= '1' after 5 ns;
    
    gen_tx:
    process
    begin
        dataRdy  <= '1' after 50 ns;
        data     <= "00100100" after 50 ns;   
        wait;
    end process gen_tx;  


end Behavioral;
