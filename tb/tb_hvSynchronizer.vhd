
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_hvSynchronizer is
end tb_hvSynchronizer;

architecture Behavioral of tb_hvSynchronizer is

     
    constant FREQ : natural := 50_000_000;  
    constant PERIOD    : time := 1 sec/FREQ; 
    
    
    component hvSynchronizer is
        generic(
            FREQ      : in natural := 50_000_000;  
            SYNCDELAY : in natural := 0
            );
        port(
            clk      : in std_logic;
            rst_n    : in std_logic;
            hSync    : out std_logic;
            vSync    : out std_logic;
            blanking : out std_logic;
            pixel    : out std_logic_vector(9 downto 0);
            line     : out std_logic_vector(9 downto 0)
        );
    end component;
    
    signal clk, rst_n : std_logic := '0';
    signal line, pixel : std_logic_vector(9 downto 0);
    signal blanking : std_logic;
    
    signal hSync, vSync :  std_logic;
    

begin

    gen_clock:
    process
    begin
        clk <= '1', '0' after PERIOD/2;
        wait for PERIOD;
    end process gen_clock;
   
    gen_rst: 
    rst_n <= '0' , '1' after 10 ns;

     screenInteface: hvSynchronizer
        generic map ( FREQ => 50_000_000, SYNCDELAY => 0 )
        port map ( clk => clk, rst_n => rst_n, blanking => blanking, pixel => pixel, line => line, hSync => hSync, vSync => vSync);

    
    
end Behavioral;
