
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity tb_fifo is

end tb_fifo;

architecture Behavioral of tb_fifo is

    constant WIDTH : natural := 8;
    constant DEPTH : natural := 3;
    
    constant FREQUENCY : real := 50.0e6; 
    constant PERIOD    : time := 1 sec/FREQUENCY; 
    
    component fifo is
    generic(
         WIDTH : in natural := 8;
         DEPTH : in natural := 5
    );
    Port ( 
        rst_n   : in std_logic;
        clk     : in std_logic;
        wr      : in std_logic;
        rd      : in std_logic;
        in_fifo : in std_logic_vector(WIDTH - 1 downto 0);  
        full    : out std_logic;
        empty   : out std_logic;
        out_fifo: out std_logic_vector(WIDTH - 1 downto 0)
    );
    end component;
    
    signal rst_n, clk, wr, rd, full, empty: std_logic := '0';
    signal in_fifo, out_fifo : std_logic_vector(WIDTH - 1 downto 0) := (others => '0');
    
    signal rand_num : integer := 0;

begin

    -- Connect UUT
    UUT: fifo
     generic map (WIDTH => WIDTH, DEPTH => DEPTH)
     port map(rst_n => rst_n, clk => clk, wr => wr, rd => rd, in_fifo => in_fifo, full => full, empty => empty, out_fifo => out_fifo);

   gen_clock:
    process
    begin
        clk <= '1', '0' after PERIOD/2;
        wait for PERIOD;
    end process gen_clock;
   
   gen_rst: 
    rst_n <= '1' after 5 ns;
    
--    gen_wr:
--    wr <= '1' after 20 ns;
--    in_fifo <= in_fifo + 1 after 20ns;
   
   gen_rd:
    wr <= '1' after 20 ns, '0' after 120 ns;
    in_fifo <= in_fifo + 1 after 20ns;
    rd <= '1' after 120 ns;


end Behavioral;
